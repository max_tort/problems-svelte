import {
  CallFunctionCommand,
  StepCommand,
  TurnLeftCommand,
  TurnRightCommand,
  PaintCommand,
  IfCommand
} from './engine'

const DEFAULTS = {
  TIMING: 500
}

function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

class If extends IfCommand {
  constructor(color, command) {
    super(color, command)
  }
  async execute(level) {
    await super.execute(level)
    return true
  }
}

class CallFunction extends CallFunctionCommand {
  constructor(CustomFunction, options = {
    timing: DEFAULTS.TIMING,
    update: () => {},
    onWin: () => (false),
    isPlaying: () => (true),
  }) {
    super(CustomFunction)
    this.timing = options.timing
    this.update = options.update
    this.onWin = options.onWin
    this.isPlaying = options.isPlaying
  }
  async execute(level) {
    ++this.counterOfRecursion;
    if (this.counterOfRecursion > this.maxNumberOfRecursionCalls)
      throw 'stack overflow'
    for (const command of this.CustomFunction) {
      if (!this.isPlaying()) return true
      const result = await command.execute(level)
      this.update()
      if (result) {
        if (level.checkGoal()) this.onWin()
      } else return false
    }
    return true
  }
}

class Step extends StepCommand {
  constructor(options = {
    timing: DEFAULTS.TIMING
  }) {
    super()
    this.timing = options.timing
  }
  async execute(level) {
    await timeout(this.timing)
    super.execute(level)
    return level.checkBounds()
  }
}

class TurnLeft extends TurnLeftCommand {
  constructor(options = {
    timing: DEFAULTS.TIMING,
  }) {
    super()
    this.timing = options.timing
  }
  async execute(level) {
    await timeout(this.timing)
    super.execute(level)
    return true
  }
}

class TurnRight extends TurnRightCommand {
  constructor(options = {
    timing: DEFAULTS.TIMING,
  }) {
    super()
    this.timing = options.timing
  }
  async execute(level) {
    await timeout(this.timing)
    super.execute(level)
    return true
  }
}

class Paint extends PaintCommand {
  constructor(color, options = {
    timing: DEFAULTS.TIMING,
  }) {
    super(color)
    this.timing = options.timing
  }
  async execute(level) {
    await timeout(this.timing)
    super.execute(level)
    return true
  }
}

const HOST = "http://134.122.95.26"
async function fetchLevel(_id) {
  let response = await fetch(`${HOST}/level/get`, {
    method: "POST",
    mode: 'cors',
    headers: {
      'accept': 'application/json',
      'Content-Type': 'application/json' //X-www-form-urlencoded
    },
    body: JSON.stringify({
      id: _id
    })
  })
  if (response.status == 200) return response.json()
  else {
    throw new Error(response.statusText);
  }
}

function translateToPx([x, y], width) {
  let deltaX = width / 2
  let deltaY = deltaX / 2
  return [
    (x - y) * deltaX,
    (y + x) * deltaY
  ]
}

export {
  fetchLevel,
  translateToPx,
  timeout,
  CallFunction,
  If,
  Step,
  Paint,
  TurnLeft,
  TurnRight
}